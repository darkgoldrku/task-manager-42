package ru.t1.bugakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConstants.TABLE_PROJECT)
public final class Project extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

}
