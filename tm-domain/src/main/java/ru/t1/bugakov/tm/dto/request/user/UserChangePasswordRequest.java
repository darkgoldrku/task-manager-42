package ru.t1.bugakov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String password;

    public UserChangePasswordRequest(@Nullable String password) {
        this.password = password;
    }

    public UserChangePasswordRequest(@Nullable String token, @Nullable String password) {
        super(token);
        this.password = password;
    }

}
