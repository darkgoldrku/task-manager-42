package ru.t1.bugakov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.service.IConnectionProvider;
import ru.t1.bugakov.tm.api.service.IDataBaseProperty;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.ISaltProvider;

import java.util.Properties;

public final class PropertyService implements IPropertyService, ISaltProvider, IConnectionProvider, IDataBaseProperty {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8800";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "127.0.0.1";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "4112700187";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10000";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jbdc:postgres://127.0.0.1:5432/taskmanager";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @Override
    @NotNull
    public int getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME_KEY, DATABASE_USERNAME_DEFAULT);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY, DATABASE_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO_KEY, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}
