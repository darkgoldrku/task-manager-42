package ru.t1.bugakov.tm.dto.response.result;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
