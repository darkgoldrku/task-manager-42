package ru.t1.bugakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConstants.TABLE_USER)
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "login")
    private String login;

    @NotNull
    @Column(nullable = false, name = "password_hash")
    private String passwordHash = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "role")
    private Role role = Role.USUAL;

    @Nullable
    @Column(name = "email")
    private String email = "";

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName = "";

    @Nullable
    @Column(name = "middle_name")
    private String middleName = "";

    @Column(name = "locked")
    private boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

}