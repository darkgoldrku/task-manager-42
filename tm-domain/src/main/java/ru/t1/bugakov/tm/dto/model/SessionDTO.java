package ru.t1.bugakov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConstants.TABLE_SESSION)
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @Nullable
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

}
