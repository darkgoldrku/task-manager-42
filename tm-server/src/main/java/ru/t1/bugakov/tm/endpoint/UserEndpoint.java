package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.IUserEndpoint;
import ru.t1.bugakov.tm.api.service.IAuthService;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.api.service.IUserService;
import ru.t1.bugakov.tm.dto.model.SessionDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.dto.request.user.*;
import ru.t1.bugakov.tm.dto.response.user.*;
import ru.t1.bugakov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService(endpointInterface = "ru.t1.bugakov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) throws SQLException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        @Nullable final UserDTO user = getUserService().findByLogin(login);
        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) throws SQLException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        @Nullable final UserDTO user = getUserService().findByLogin(login);
        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) throws SQLException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user = getUserService().findByLogin(login);
        getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) throws SQLException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        getUserService().updateUser(userId, firstName, lastName, middleName);
        @Nullable final UserDTO user = getUserService().findById(userId);
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) throws SQLException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        getUserService().setPassword(userId, password);
        @Nullable final UserDTO user = getUserService().findById(userId);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) throws SQLException {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final UserDTO user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

}
