package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IAbstractUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IAbstractUserOwnedRepository<M> {


}
