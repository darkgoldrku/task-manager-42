package ru.t1.bugakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.api.service.IAbstractService;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.dto.model.AbstractModelDTO;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModelDTO, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    protected AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected SqlSession getSqlSession() {
        return connectionService.getSqlSession();
    }

    @NotNull
    protected abstract IAbstractRepository<M> getRepository(@NotNull final SqlSession sqlSession);

    @Override
    public void add(@Nullable final M model) throws SQLException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.add(model);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@NotNull final Collection<M> models) throws SQLException {
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.add(models);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*@NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws SQLException {
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.set(models);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return models;
    }*/

    @NotNull
    @Override
    public List<M> findAll() throws SQLException {
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<M> findAllWithSort(@Nullable final Comparator<M> comparator) throws SQLException {
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            if (comparator == null) return findAll();
            return repository.findAllWithSort(comparator);
        }
    }

    @Override
    public int getSize() throws SQLException {
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            return repository.getSize();
        }
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            return repository.findById(id);
        }
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final Integer index) throws SQLException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            return repository.findByIndex(index);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            return repository.existsById(id);
        }
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.clear();
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final M model) throws SQLException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.remove(model);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.removeById(id);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) throws SQLException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.removeByIndex(index);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<M> models) throws SQLException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.removeAll(models);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
