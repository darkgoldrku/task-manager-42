package ru.t1.bugakov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.api.service.IAbstractUserOwnedService;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.bugakov.tm.enumerated.SortType;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IAbstractUserOwnedRepository<M>> extends AbstractService<M, R> implements IAbstractUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull SqlSession getSqlSession() {
        return connectionService.getSqlSession();
    }

    @NotNull
    public abstract IAbstractUserOwnedRepository<M> getRepository(@NotNull final SqlSession session);

    @Override
    public void add(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            IAbstractUserOwnedRepository<M> repository = getRepository(session);
            repository.add(userId, model);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    public List<M> findAllWithSort(@Nullable final String userId, @Nullable String sortField) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (sortField == null) return findAll(userId);
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            @Nullable final SortType sortType = SortType.toSort(sortField);
            @Nullable String columnForSort = "";
            if (sortType == null) return findAll(userId);
            switch ((sortType)) {
                case BY_NAME:
                    columnForSort = DBConstants.COLUMN_NAME;
                    break;
                case BY_STATUS:
                    columnForSort = DBConstants.COLUMN_STATUS;
                    break;
                case BY_CREATED:
                    columnForSort = DBConstants.COLUMN_CREATED;
                    break;
            }
            return repository.findAllWithSort(userId, columnForSort);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            return repository.getSize(userId);
        }
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            return repository.findById(userId, id);
        }
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            return repository.findByIndex(userId, index);
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            return repository.existsById(userId, id);
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            repository.clear(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(session);
            repository.remove(userId, model);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            IAbstractUserOwnedRepository<M> repository = getRepository(session);
            repository.removeById(userId, id);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            IAbstractUserOwnedRepository<M> repository = getRepository(session);
            repository.removeByIndex(userId, index);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId, @Nullable final Collection<M> models) throws SQLException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull SqlSession session = getSqlSession();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(session);
            repository.removeAll(models);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
