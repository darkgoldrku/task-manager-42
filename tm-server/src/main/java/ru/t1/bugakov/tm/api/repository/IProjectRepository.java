package ru.t1.bugakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IAbstractUserOwnedRepository<ProjectDTO> {


    @Override
    @Insert("INSERT INTO " + DBConstants.TABLE_PROJECT + " (id, user_id, name, description, status, created) " +
            "VALUES (#{id},#{userId},#{name},#{description},#{status},#{created})")
    void add(@NotNull ProjectDTO project) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{sortField}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllWithSort(final @NotNull @Param("userId") String userId, @Nullable final @Param("sortField") String sortField) throws SQLException;

    @Override
    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userID}")
    int getSize(final @NotNull @Param("userId") String userId) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "id", column = "id")
    })
    ProjectDTO findById(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index - 1}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findByIndex(final @NotNull @Param("userId") String userId, final @NotNull @Param("index") Integer index) throws SQLException;

    @Select("SELECT EXISTS (SELECT id FROM tm_project WHERE user_id = #{userId} AND id = #{id})::boolean")
    boolean existsById(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_project WHERE user_id = #{user_id}")
    void clear(final @NotNull @Param("user_id") String userId) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_project WHERE user_id = #{user_id} AND id = #{id}")
    void remove(final @Nullable @Param("user_id") String userId, final @Nullable ProjectDTO model) throws SQLException;

    @Update("UPDATE tm_project SET user_id = #{userId}, project_id = #{projectId}, name = #{name}, description = #{description}, status = #{status}, created = #{created} WHERE id = #{id}")
    void update(@NotNull ProjectDTO project) throws SQLException;
}
