package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.dto.model.AbstractModelDTO;

public interface IAbstractService<M extends AbstractModelDTO> extends IAbstractRepository<M> {

}
