package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataXmlLoadJaxBRequest;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA Load XML]");
        getDomainEndpoint().XmlLoadJaxB(new DataXmlLoadJaxBRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-load-xml-jaxb";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from xml file with jaxb";
    }

}
