package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.model.TaskDTO;
import ru.t1.bugakov.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;
import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<TaskDTO> tasks = getTaskEndpoint().listTasksByProjectId(new TaskListByProjectIdRequest(getToken(), projectId)).getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show tasks by project id.";
    }

}
