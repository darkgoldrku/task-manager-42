package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.dto.request.project.ProjectGetByIndexRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectDTO project = getProjectEndpoint().getProjectByIndex(new ProjectGetByIndexRequest(getToken(), index)).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

}
