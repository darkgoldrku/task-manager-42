package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.bugakov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.dto.request.project.*;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.dto.response.project.ProjectListResponse;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.marker.SoapCategory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private List<ProjectDTO> projectList;

    @Nullable
    private String token;

    @Before
    public void initEndpoint() throws SQLException {
        projectList = new ArrayList<>();
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        @Nullable final String projectId = projectEndpoint.createProject(new ProjectCreateRequest(token, "ClientTestProject", "ClientTestProjectDescription")).getProject().getId();
        projectList.add(projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, projectId)).getProject());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusById() throws SQLException {
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final ProjectDTO projectForUpdate = projectList.get(0);
        projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(token, projectForUpdate.getId(), newStatus));
        @Nullable final ProjectDTO projectAfterUpdate = projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, projectForUpdate.getId())).getProject();
        Assert.assertEquals(newStatus, projectAfterUpdate.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByIndex() throws SQLException {
        @NotNull final Status newStatus = Status.COMPLETED;
        projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(token, 0, newStatus));
        @Nullable final ProjectDTO projectAfterUpdate = projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, 0)).getProject();
        Assert.assertEquals(newStatus, projectAfterUpdate.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearProjects() throws SQLException {
        projectEndpoint.clearProjects(new ProjectClearRequest(token));
        ProjectListResponse response = projectEndpoint.listProjects(new ProjectListRequest(token, null));
        Assert.assertNull(response.getProjects());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateProject() throws SQLException {
        int size = projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size();
        projectEndpoint.createProject(new ProjectCreateRequest(token, "TestTaskAdd", "TestDescriptionAdd"));
        Assert.assertEquals(size + 1, projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectByIdPositive() throws SQLException {
        @NotNull final ProjectDTO projectForSearch = projectList.get(0);
        Assert.assertNotNull(projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, projectForSearch.getId())));
        @Nullable final ProjectDTO taskSearch = projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, projectForSearch.getId())).getProject();
        Assert.assertEquals(projectForSearch.getId(), taskSearch.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectByIdNegative() throws SQLException {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, id)).getProject());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectByIndex() throws SQLException {
        Assert.assertNotNull(projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, 0)).getProject());
        int size = projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size();
        Assert.assertNull(projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, size)).getProject());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListProjects() throws SQLException {
        Assert.assertNotNull(projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectById() throws SQLException {
        int size = projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, projectList.get(0).getId()));
        Assert.assertEquals(size - 1, projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndex() throws SQLException {
        int size = projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size();
        projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, 0));
        Assert.assertEquals(size - 1, projectEndpoint.listProjects(new ProjectListRequest(token, null)).getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectById() throws SQLException {
        @NotNull final String newName = "TestTaskUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @NotNull final ProjectDTO projectForUpdate = projectList.get(0);
        @Nullable final ProjectDTO projectAfterUpdate = projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(token, projectForUpdate.getId(), newName, newDescription)).getProject();
        Assert.assertEquals(newName, projectAfterUpdate.getName());
        Assert.assertEquals(newDescription, projectAfterUpdate.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndex() throws SQLException {
        @NotNull final String newName = "TestTaskUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @Nullable final ProjectDTO projectAfterUpdate = projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(token, 0, newName, newDescription)).getProject();
        Assert.assertEquals(newName, projectAfterUpdate.getName());
        Assert.assertEquals(newDescription, projectAfterUpdate.getDescription());
    }

}
